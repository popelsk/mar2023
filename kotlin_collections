//introduction 

fun Shop.getSetOfCustomers(): Set<Customer> =
        customers.toSet()




//Sort
fun Shop.getCustomersSortedByOrders(): List<Customer> =
        customers.sortedByDescending {it.orders.size}


//Filter map
fun Shop.getCustomerCities(): Set<City> =
        customers.map {it.city}.toSet()

fun Shop.getCustomersFrom(city: City): List<Customer> =
        customers.filter {it.city == city}



//All Any and other predicates

// Return true if all customers are from a given city
fun Shop.checkAllCustomersAreFrom(city: City): Boolean =
        customers.all { it.city == city }

// Return true if there is at least one customer from a given city
fun Shop.hasCustomerFrom(city: City): Boolean =
        customers.any { it.city == city }

// Return the number of customers from a given city
fun Shop.countCustomersFrom(city: City): Int =
        customers.count { it.city == city }

// Return a customer who lives in a given city, or null if there is none
fun Shop.findCustomerFrom(city: City): Customer? =
        customers.find { it.city == city }




//Associate

// Build a map from the customer name to the customer
fun Shop.nameToCustomerMap(): Map<String, Customer> =
        customers.associateBy(Customer::name)

// Build a map from the customer to their city
fun Shop.customerToCityMap(): Map<Customer, City> =
        customers.associateWith(Customer::city)

// Build a map from the customer name to their city
fun Shop.customerNameToCityMap(): Map<String, City> =
	customers.associate { it.name to it.city }






//GroupBy

// Build a map that stores the customers living in a given city
fun Shop.groupCustomersByCity(): Map<City, List<Customer>> =
        customers.groupBy { it.city }



//Partition
// Return customers who have more undelivered orders than delivered
fun Shop.getCustomersWithMoreUndeliveredOrders(): Set<Customer> =  customers.filter {
    val (delivered, undelivered) = it.orders.partition { it.isDelivered }
    undelivered.size > delivered.size
}.toSet()



//FlatMap
// Return all products the given customer has ordered
fun Customer.getOrderedProducts(): List<Product> =
        orders.flatMap(Order::products)

// Return all products that were ordered by at least one customer
fun Shop.getOrderedProducts(): Set<Product> =
        customers.flatMap(Customer::getOrderedProducts).toSet()




//Max min
// Return a customer who has placed the maximum amount of orders
fun Shop.getCustomerWithMaxOrders(): Customer? =
        customers.maxByOrNull { it.orders.size }

// Return the most expensive product that has been ordered by the given customer
fun getMostExpensiveProductBy(customer: Customer): Product? =
        customer.orders
                .flatMap(Order::products)
                .maxByOrNull(Product::price)


//Sum
fun moneySpentBy(customer: Customer): Double {
    return customer.orders.flatMap { it.products.map { it.price } }.sum()
}



//Fold and reduce
// Return the set of products that were ordered by all customers
fun Shop.getProductsOrderedByAll(): Set<Product> =
    customers.fold(customers.first().getOrderedProducts()) { acc, customer ->
        acc.intersect(customer.getOrderedProducts())
    }

fun Customer.getOrderedProducts(): Set<Product> =
    orders.flatMap { it.products }.toSet()
    


//Compound tasks
fun findMostExpensiveProductBy(customer: Customer): Product? {
    return customer.orders
        .filter { it.isDelivered }
        .flatMap { it.products }
        .maxByOrNull { it.price }
}

fun Shop.getNumberOfTimesProductWasOrdered(product: Product): Int {
    return customers.flatMap { it.getOrderedProducts() }
        .count { it == product }
}

fun Customer.getOrderedProducts(): List<Product> {
    return orders.flatMap { it.products }
}



//Sequences
fun findMostExpensiveProductBy(customer: Customer): Product? {
    return customer.orders.asSequence()
        .filter { it.isDelivered }
        .flatMap { it.products.asSequence() }
        .maxByOrNull { it.price }
}

fun Shop.getNumberOfTimesProductWasOrdered(product: Product): Int {
    return customers.asSequence()
        .flatMap { it.getOrderedProducts() }
        .count { it == product }
}

fun Customer.getOrderedProducts(): Sequence<Product> {
    return orders.asSequence().flatMap { it.products.asSequence() }
}


//Getting used to new style
fun doSomethingWithCollection(collection: Collection<String>): Collection<String>? {

    val groupsByLength = collection.groupBy { it.length }

    val maximumSizeOfGroup = groupsByLength.values.map { it.size }.maxOrNull()

    return groupsByLength.values.firstOrNull { it.size == maximumSizeOfGroup }
}
